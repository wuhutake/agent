#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/wait.h>


int conn;
char sbuf[512];

void raw(char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(sbuf, 512, fmt, ap);
    va_end(ap);
    printf("<< %s", sbuf);
    write(conn, sbuf, strlen(sbuf));
}

int main() {
    
    char *nick = "agent";
    char *greeting = "привет";
    char *channel = "#christ"; 
    char *host = "efnet.port80.se";
    char *port = "6667";
    
    char *user, *command, *where, *message, *sep, *target, *output;
    int i, j, l, sl, o = -1, start, wordcount;
    char buf[513];
    struct addrinfo hints, *res;
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(host, port, &hints, &res);
    conn = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    connect(conn, res->ai_addr, res->ai_addrlen);
    
    raw("USER %s 0 0 :%s\r\n", nick, nick);
    raw("NICK %s\r\n", nick);
    
 while ((sl = read(conn, sbuf, 512)))
   {
     for (i = 0; i < sl; i++)
       {
	 o++;
	 buf[o] = sbuf[i];
	 if ((i > 0 && sbuf[i] == '\n' && sbuf[i - 1] == '\r') || o == 512)
	   {
	     buf[o + 1] = '\0';
	     l = o;
	     o = -1;
                
	     printf(">> %s", buf);
                
	     if (!strncmp(buf, "PING", 4))
	       {
		 buf[1] = 'O';
		 raw(buf);
                }
	     else if (buf[0] == ':')
	       {
		 wordcount = 0;
		 user = command = where = message = NULL;
		 for (j = 1; j < l; j++)
		   {
		     if (buf[j] == ' ')
		       {
			 buf[j] = '\0';
			 wordcount++;
			 switch(wordcount)
			   {
			   case 1: user = buf + 1; break;
			   case 2: command = buf + start; break;
			   case 3: where = buf + start; break;
			   }
			 if (j == l - 1) continue;
			 start = j + 1;
                        }
		     else if (buf[j] == ':' && wordcount == 3)
		       {
			 if (j < l - 1) message = buf + j + 1;
			 break;
		       }
		   }
		 
		 if (wordcount < 2) continue;
                 
		 if (!strncmp(command, "001", 3) && channel != NULL)
		   {
		     raw("JOIN %s\r\n", channel);
		     raw("PRIVMSG %s :%s\r\n", channel, greeting);
		   }
		 else if (!strncmp(command, "PRIVMSG", 7) || !strncmp(command, "NOTICE", 6))
		   {
		     if (where == NULL || message == NULL) continue;
		     if ((sep = strchr(user, '!')) != NULL) user[sep - user] = '\0';
		     if (where[0] == '#' || where[0] == '&' || where[0] == '+' || where[0] == '!') target = where; else target = user;
		     printf("[from: %s] [reply-with: %s] [where: %s] [reply-to: %s] %s", user, command, where, target, message);
		     
		     if (user != nick && !strncmp(message, ">", 1))
		       {
			 
			 message = message + 1;
			 int r,t;
			 char *cmd;
			 char p = '|';
			 char* sarray[128][128];
			 char *saveptr;
			 char *sp2;
			 char *sp3;
			 char* pipearray[128];
			 int x =0;
			 int z=0;
			 int y = 0;

			 if (strchr(message,p))
			   {
			     char *tok2;
			     i=0;
			     char *str =  strtok_r(message,"|",&sp2);
			     // aaa | bb | ccc
			     while (str != NULL)
			       {
				 pipearray[i] = str;
				 str = strtok_r(NULL,"|",&sp2);
				 i++;
			       }
			     for (z=0;z<i;z++)
			       {
				 tok2 = strtok_r(pipearray[z]," \r\n", &sp3);
				 y=0;
				 while (tok2 != NULL)
				   {
				     sarray[z][y] = tok2;
				     tok2 = strtok_r(NULL," \r\n", &sp3);
				     y++;
				   }
			       }
			     sarray[z][y] = (char*)NULL;
			   }
		   
			 else
			   { 
			     char *split = strtok_r(message," \r\n",&saveptr);
			     i=0; 
			     while (split != NULL)
			       {  			  
				 sarray[0][i] = split; 
				 split = strtok_r(NULL," \r\n", &saveptr); 
				 i++;
				 
			       }
			     sarray[0][i] = (char*)NULL; 
			     z=1;
			   }
			 
			 int fd_pipe[2];
			 pid_t pid;
			 int status;
			 int input = 0;
			 
			 for (t=0;t<z;t++)
			   {
			     pipe(fd_pipe);
			     pid = fork();
			     
			     if (pid == 0)
			       { //CHILD
				 if (t == 0)
				   {
				     close(fd_pipe[0]);
				     dup2(fd_pipe[1], 1);
				   }
				 else
				   {
				     
				     dup2(input, 0);   //read from last pipe
				     dup2(fd_pipe[1], 1); // stdout to pipe
				     close(fd_pipe[0]);
				   }
				 
				 execvp(sarray[t][0], sarray[t]);  
				 perror("execvp");
				 exit(1);
			       }
			     
			     else if (pid > 0)
			       {//PARENT
				 close(fd_pipe[1]);
				 input = fd_pipe[0]; //save input for next fork
				 wait(&status);
			       }
			     
			     else
			       {
				 perror("fork");
				 exit(1);
			       }
			   }
		   
			 char buffer[513];  
			 while (1)
			   {
			     ssize_t count  = read(fd_pipe[0], buffer, sizeof(buffer));
			     if (count == -1)
			       {
				 if (errno == EINTR)
				   {
				     continue;
				   }
				 else
				   {
				     perror("read");
				     exit(1);
				   }
			       }
			     else if (count == 0)
			       {
				 break;
			       }
			     else
			       {
				 char *sp;
				 char* split2=strtok_r(buffer,"\n",&sp);
				 while(split2 != NULL)
				   {
				     raw("PRIVMSG %s :%s\r\n", target, split2);
				     split2 = strtok_r(NULL,"\n",&sp);
				   }
			       }
			   }
			 wait(0);	
			 memset(buffer,0,128);
		       }
		   }
	       }
	   }
       }
   }
 return 0;
}
